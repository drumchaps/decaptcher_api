from distutils.core import setup

setup(
    name="decaptcher_api",
    version="0.2.0",
    author="Chaps",
    author_email="drumchaps@gmail.com",
    maintainer="Chaps",
    maintainer_email="drumchaps@gmail.com",
    url="https://bitbucket.org/drumchaps/decaptcherapi",
    packages  = [
        "decaptcher_api",
    ],
    package_dir={'': 'src'},
    install_requires = ["requests",]
)


